defmodule Football.FootballService do
  use GenServer

  # Client

  def start_link(file_path) when is_binary(file_path) do
    GenServer.start_link(__MODULE__, file_path, name: FootballService)
  end

  def get_pairs() do
    GenServer.call(FootballService, :get_pairs)
  end

  def get_pair_results(pair) do
    GenServer.call(FootballService, {:get_pair_results, pair})
  end

  # Server (callbacks)

  @impl true
  def init(file_path) do

    :ets.new(:pair_results, [:named_table, :bag, :public])

    unique_pairs =

    file_path
    |>  Football.CsvParser.parse()
    |>  List.foldl(%MapSet{}, fn game, acc ->

          :ets.insert(:pair_results, {{game.div, game.season}, game})

          MapSet.put(acc, {game.div, game.season})

        end)
    |>  MapSet.to_list

    {:ok, %{unique_pairs: unique_pairs}}
  end

  @impl true
  def handle_call(:get_pairs, _from, %{:unique_pairs => unique_pairs} = state) do
    {:reply, unique_pairs, state}
  end

  @impl true
  def handle_call({:get_pair_results, {division, season}}, _from, state) do
    resp =
    if :ets.member(:pair_results, {division, season}) do
      :ets.lookup_element(:pair_results, {division, season}, 2)
    end
    {:reply, resp, state}
  end

end