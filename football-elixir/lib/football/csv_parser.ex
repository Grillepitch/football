defmodule Football.CsvParser do

  alias NimbleCSV.RFC4180, as: CSV

  def str_to_int(str) do
  	case Integer.parse(str) do
  		{val, _} -> val
      	:error -> :error		
  	end
  end

  def parse(), do: parse("./priv/data.csv")
  def parse(file) do
  	file
	|> File.stream!
	|> CSV.parse_stream
	|> Stream.map(fn [id, div, season, date, home_team, away_team, fthg, ftag, ftr, hthg, htag, htr] ->
	  %{id: id,
	  	div: div,
	    season: season,
		date: date,
		home_team: home_team,
		away_team: away_team,
		fthg: str_to_int(fthg),
		ftag: str_to_int(ftag),
		ftr: ftr,
		hthg: str_to_int(hthg),
		htag: str_to_int(htag),
		htr: htr
	}
	end)
	|> Enum.to_list
  end
end
