defmodule FootballWeb.PairsController do
  use FootballWeb, :controller

  def index(conn, _params) do

  	resp = GenServer.call(FootballService, :get_pairs)

    render conn, :index, pairs: resp
  end
end
