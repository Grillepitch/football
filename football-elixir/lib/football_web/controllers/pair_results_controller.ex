defmodule FootballWeb.PairResultsController do
  use FootballWeb, :controller

  def show(conn, %{"division" => division, "season" => season}) do
    
    resp = GenServer.call(FootballService, {:get_pair_results, {division, season}})

    render conn, :show, pair_results: resp
  end
end
