defmodule FootballWeb.Protobufs do
  use Protobuf, """

      message Pair {
        string division = 100;
        string season = 200;
      }

      message Pairs {
        repeated Pair results = 100;
      }

      message PairResult {
        string division = 100;
        string season = 200;
        string date = 300;
        string home_team = 400;
        string away_team = 500;
        int32 fthg = 600;
        int32 ftag = 700;
        string ftr = 800;
        int32 hthg = 900;
        int32 htag = 1000;
        string htr = 1100;
      }

      message PairResults {
        repeated PairResult results = 100;
      }
  """

  def encode_to_iodata!(%{__struct__: protobuf_module} = data) do
    protobuf_module.encode(data)
  end

end