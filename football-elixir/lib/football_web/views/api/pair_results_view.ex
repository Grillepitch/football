defmodule FootballWeb.PairResultsView do
  use FootballWeb, :view
  alias FootballWeb.Protobufs

  def render("show.json", %{pair_results: pair_results}) do
    %{
      results: pair_results
    }
  end

  def render("show.proto", %{pair_results: pair_results}) do
    Protobufs.PairResults.new(
      results: pair_results |> Enum.map(fn (pair_result) ->
        pair_result
        |> Protobufs.PairResult.new()
      end)
    )
  end
end