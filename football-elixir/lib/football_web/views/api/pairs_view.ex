defmodule FootballWeb.PairsView do
  use FootballWeb, :view
  alias FootballWeb.Protobufs

  def render("index.json", %{pairs: pairs}) do
    %{
      results: render_many(pairs, __MODULE__, "show.json", as: :pair)
    }
  end

  def render("show.json", %{pair: {division, season}}) do
    %{division: division, season: season}
  end

  def render("index.proto", %{pairs: pairs}) do
    Protobufs.Pairs.new(
      results: render_many(pairs, __MODULE__, "show.proto", as: :pair)
    )
  end

  def render("show.proto", %{pair: {division, season}}) do
    Protobufs.Pair.new(%{division: division, season: season})
  end
end