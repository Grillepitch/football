defmodule FootballWeb.Router do
  use FootballWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json", "proto"]
  end

  # scope "/", FootballWeb do
  #   pipe_through :browser # Use the default browser stack

  #   get "/", PageController, :index
  # end

  # Other scopes may use custom stacks.
  scope "/public/api/v1", FootballWeb do
    pipe_through :api

    get "/pairs", PairsController, :index
    get "/div/:division/season/:season", PairResultsController, :show
  end
end
