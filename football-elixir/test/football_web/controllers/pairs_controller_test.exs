defmodule Football.PairsControllerTest do
  use FootballWeb.ConnCase

  test "json", %{conn: conn} do
    resp = build_conn()
           |> get(pairs_path(conn, :index, %{"_format" => "json"}))
           |> doc
    data = json_response(resp, 200)

    assert data == %{"results" => [%{"season" => "201617", "division" => "D1"},
                                   %{"division" => "SP1", "season" => "201617"}]
                    }
  end
end
