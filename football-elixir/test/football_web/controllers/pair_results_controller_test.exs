defmodule Football.PairResultsControllerTest do
  use FootballWeb.ConnCase

  test "json", %{conn: conn} do
	resp = build_conn()
	 		|> get(pair_results_path(conn, :show, "SP1", "201617", %{"_format" => "json"})) 
	 		|> doc
    data = json_response(resp, 200)

    assert data == %{"results" => 
    					[%{"div" => "SP1", 
    					   "hthg" => 0, 
    					   "away_team" => "Sevilla", 
    					   "date" => "28/08/16", 
    					   "ftag" => 0, 
    					   "fthg" => 0, 
    					   "ftr" => "D", 
    					   "home_team" => "Villarreal", 
    					   "htag" => 0, 
    					   "htr" => "D", 
    					   "id" => "20", 
    					   "season" => "201617"
    					  },
    					 %{"fthg" => 1, 
    					   "htag" => 0,
    					   "away_team" => "Espanol",
    					   "date" => "09/09/16", 
    					   "div" => "SP1", 
    					   "ftag" => 1, 
    					   "ftr" => "D", 
    					   "home_team" => "Sociedad", 
    					   "hthg" => 0, 
    					   "htr" => "D", 
    					   "id" => "21", 
    					   "season" => "201617"
    					  }]
    				}
  end
end