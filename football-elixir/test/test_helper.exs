ExUnit.start()

# Ecto.Adapters.SQL.Sandbox.mode(Football.Repo, :manual)
Bureaucrat.start(
	writer: Bureaucrat.MarkdownWriter,
  	default_path: "./doc/API.md"
  )
ExUnit.start(formatters: [ExUnit.CLIFormatter, Bureaucrat.Formatter])