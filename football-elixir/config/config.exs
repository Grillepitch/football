# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
# config :football,
  # ecto_repos: [Football.Repo]

# Configures the endpoint
config :football, FootballWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "+siPLDuLUQi7LmttIESOyX4nfsp4jSRCwZhru1/TZIY7xr3jCVxks+r0cfcwT1r4",
  render_errors: [view: FootballWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Football.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]


config :phoenix, :format_encoders,
  proto: FootballWeb.Protobufs


config :football, Football.InfluxdbConnection,
  database:  "football_metrics",
  host: "localhost",
  http_opts: [insecure: true],
  pool: [max_overflow: 1, size: 2],
  port: 8086,
  scheme: "http",
  writer: Instream.Writer.Line


# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
