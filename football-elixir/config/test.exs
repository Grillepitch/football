use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :football, FootballWeb.Endpoint,
  http: [port: 4001],
  server: false

# config :football, Football.FootballService, ["./test/fixtures/data.csv"]

# Print only warnings and errors during test
config :logger, level: :warn

config :football, :path_to_csv, "./test/fixtures/data.csv"