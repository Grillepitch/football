cd football-elasticsearch && docker build -t football-elasticsearch .
cd ../football-fluentd && docker build -t football-fluentd .
cd ../football-kibana && docker build -t football-kibana .
cd ../football-influxdb && docker build -t football-influxdb .
cd ../football-grafana && docker build -t football-grafana .
cd ../football-elixir && docker build -t football-elixir .
cd ..
docker swarm init
docker stack deploy --compose-file=docker-compose.yml prod