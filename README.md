# Install And Run

* run_me.sh

# API Documentation

  * [FootballWeb.PairResultsController](#footballweb-pairresultscontroller)
    * [show](#footballweb-pairresultscontroller-show)
  * [FootballWeb.PairsController](#footballweb-pairscontroller)
    * [index](#footballweb-pairscontroller-index)

## FootballWeb.PairResultsController
#### json
##### Request
* __Method:__ GET
* __Path:__ /public/api/v1/div/SP1/season/201617?_format=json

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "results": [
    {
      "season": "201617",
      "id": "20",
      "htr": "D",
      "hthg": 0,
      "htag": 0,
      "home_team": "Villarreal",
      "ftr": "D",
      "fthg": 0,
      "ftag": 0,
      "div": "SP1",
      "date": "28/08/16",
      "away_team": "Sevilla"
    },
    {
      "season": "201617",
      "id": "21",
      "htr": "D",
      "hthg": 0,
      "htag": 0,
      "home_team": "Sociedad",
      "ftr": "D",
      "fthg": 1,
      "ftag": 1,
      "div": "SP1",
      "date": "09/09/16",
      "away_team": "Espanol"
    }
  ]
}
```

## FootballWeb.PairsController
#### json
##### Request
* __Method:__ GET
* __Path:__ /public/api/v1/pairs?_format=json

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "results": [
    {
      "season": "201617",
      "division": "D1"
    },
    {
      "season": "201617",
      "division": "SP1"
    }
  ]
}
```

